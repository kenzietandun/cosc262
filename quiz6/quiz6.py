#!/usr/bin/env python3

def change_greedy(amount, coinage):
    coinage_ordered = sorted(coinage, reverse=True)
    leftover = amount
    results = []
    for coin in coinage_ordered:
        coin_amount = leftover // coin
        leftover -= coin_amount * coin
        if coin_amount > 0:
            results.append((coin_amount, coin))

    if leftover > 0:
        return None
    return results

# 140 2 == 70
# 150 3 == 50
# 160 4 == 40
# 180 3 == 60

# max 7 kg
# 140 + 180 + 2/3 * 150
# 320 + 100

def fractional_knapsack(capacity, items):
    items_sorted = sorted(items, key=lambda x: x[1]/x[2], reverse=True)
    leftover_capacity = capacity
    knapsack_value = 0
    for _, value, weight in items_sorted:
        if leftover_capacity > weight:
            leftover_capacity -= weight
            knapsack_value += value
        else:
            knapsack_value += value * leftover_capacity / weight
            break

    return knapsack_value

def print_shows(show_list):
    show_sorted = sorted(show_list, key=lambda x: x[1] + x[2])
    ending_time = 0
    for show, start, duration in show_sorted:
        if start >= ending_time:
            end = start + duration
            ending_time = end
            print("{}, {}, {}".format(show, start, end))











class Node:
    """Represents an internal node in a Huffman tree. It has a frequency count
       and left and right subtrees, assumed to be the '0' and '1' children
       respectively.
    """
    def __init__(self, count, left, right):
        self.count = count
        self.left = left
        self.right = right
        self.min_char = min(self.left.min_char, self.right.min_char)

    def __str__(self, level=0):
        return ((2 * level) * ' ' + f"Node({self.count},\n" +
            self.left.__str__(level + 1) + ',\n' +
            self.right.__str__(level + 1) + ')')

    def __lt__(self, other):
        return self.min_char < other.min_char

    def is_leaf(self):
        return False

class Leaf:
    """A leaf node in a Huffman encoding tree. Contains a character and its
       frequency count.
    """
    def __init__(self, count, char):
        self.count = count
        self.char = char
        self.min_char = char

    def __str__(self, level=0):
        return (level * 2) * ' ' + f"Leaf({self.count}, '{self.char}')"

    def __lt__(self, other):
        return self.min_char < other.min_char

    def is_leaf(self):
        return True

import heapq
def huffman_tree(frequencies):
    heap = []
    for char, freq in frequencies.items():
        heapq.heappush(heap, (freq, Leaf(freq, char)))

    while len(heap) > 1:
        left_freq, left_leaf = heapq.heappop(heap)
        right_freq, right_leaf = heapq.heappop(heap)

        par_freq = left_freq + right_freq
        par_node = Node(par_freq, left_leaf, right_leaf)
        if left_freq == right_freq:
            if left_leaf.min_char > right_leaf.min_char:
                par_node = Node(par_freq, right_leaf, left_leaf)

        heapq.heappush(heap, (par_freq, par_node))

    _, final_tree = heap[0]
    return final_tree

# The example from the notes
freqs = {'a': 9,
         'b': 8,
         'c': 15,
         'd': 3,
         'e': 5,
         'f': 2}
print(huffman_tree(freqs))

# Example from Q11
freqs = {
   'p': 27,
   'q': 11,
   'r': 27,
   'u': 8,
   't': 5,
   's': 3}
print(huffman_tree(freqs))


def huffman_encode(s, huffman_tree):
    char_dict = parse_tree(huffman_tree)
    result = ""
    for char in s:
        result += char_dict[char]

    return result

def parse_tree(huffman_tree, binary_repr="", char_dict=dict()):
    """parses huffman tree and returns dictionary of char to their binary
    representation"""
    if huffman_tree.is_leaf():
        char_dict[huffman_tree.char] = binary_repr
    else:
        parse_tree(huffman_tree.left, binary_repr=binary_repr+'0', char_dict=char_dict)
        parse_tree(huffman_tree.right, binary_repr=binary_repr+'1', char_dict=char_dict)

    return char_dict

def huffman_decode(s, tree):
    """Takes a string s of zeros and ones and a Huffman Coding tree tree. 
    It is assumed that the string s has been encoded using the given tree 
    and this function must decode it again to yield the original string."""

    return huffman_decode_helper(s, tree, tree)

def huffman_decode_helper(s, tree, orig_tree, position=0):
    if tree.is_leaf():
        return tree.char + huffman_decode_helper(s, orig_tree, orig_tree, position=position)
    elif position >= len(s):
        return ""
    else:
        current_char = s[position]
        if current_char == '0':
            return huffman_decode_helper(s, tree.left, orig_tree, position=position+1)
        else:
            return huffman_decode_helper(s, tree.right, orig_tree, position=position+1)
