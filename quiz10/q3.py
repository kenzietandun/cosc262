# Do not alter the next two lines
from collections import namedtuple
Node = namedtuple("Node", ["value", "left", "right"])

# Rewrite the following function to avoid slicing
def search_tree(nums, is_sorted=False, start=0, end=None):
    """Return a balanced binary search tree with the given nums
       at the leaves. is_sorted is True if nums is already sorted.
       Inefficient because of slicing but more readable.
    """

    if not is_sorted:
        nums = sorted(nums)
    if end is None: 
        end = len(nums) - 1
    if start >= end:
        tree = Node(nums[start], None, None)
    elif end - start == 1:
        tree = Node(nums[start], 
                Node(nums[start], None, None), 
                Node(nums[end], None, None)
                )
    else:
        mid = (start + end) // 2
        if (end - start) % 2 == 1:
            mid = (start + end + 1) // 2
        left = search_tree(nums, True, start=start, end=mid-1)
        right = search_tree(nums, True, start=mid, end=end)
        tree = Node(nums[mid-1], left, right)
    return tree
    
# Leave the following function unchanged
def print_tree(tree, level=0):
    """Print the tree with indentation"""
    if tree.left is None and tree.right is None: # Leaf?
        print(2 * level * ' ' + f"Leaf({tree.value})")
    else:
        print(2 * level * ' ' + f"Node({tree.value})")
        print_tree(tree.left, level + 1)
        print_tree(tree.right, level + 1)
