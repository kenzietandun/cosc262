#!/usr/bin/env python3

def longest_common_substring(s1, s2):
    """finds the longest common substring using bottom up dp"""
    s1_length = len(s1)
    s2_length = len(s2)
    substring = [[0 for i in range(s2_length + 1)] for j in range(s1_length + 1)] 
    for i in range(s1_length + 1):
        for j in range(s2_length + 1):
            if i == 0 or j == 0:
                continue

            length_top = substring[i-1][j]
            length_left = substring[i][j-1]
            if s1[i-1] == s2[j-1]:
                substring[i][j] = substring[i-1][j-1] + 1
            elif length_top > length_left:
                substring[i][j] = substring[i-1][j]
            else:
                substring[i][j] = substring[i][j-1]

    return traceback(substring, s1, s2)

def traceback(substring, s1, s2):
    """traces back from the resulting substring grid
    to construct longest common string"""
    i = len(s1)
    j = len(s2)

    common_string = ""
    
    while substring[i][j] != 0:
        if s1[i-1] == s2[j-1]:
            common_string = s1[i-1] + common_string
            i -= 1
            j -= 1
        else:
            if substring[i-1][j] == substring[i][j]:
                i -= 1
            else:
                j -= 1

    return common_string
