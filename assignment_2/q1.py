#!/usr/bin/env python3

def longest_common_substring(s1, s2, pos1=0, pos2=0, cache=None):
    """finds the longest common substring using top down DP"""
    # initialize the cache
    if cache is None:
        cache = [[None for i in range(len(s2))] for j in range(len(s1))]

    # base case
    if pos1 >= len(s1) or pos2 >= len(s2):
        return ""
    elif cache[pos1][pos2] is not None:
        return cache[pos1][pos2]
    # if they have the same letter
    elif s1[pos1] == s2[pos2]:
        return s1[pos1] + longest_common_substring(s1, s2, pos1+1, pos2+1, cache)
    else:
        left = longest_common_substring(s1, s2, pos1+1, pos2, cache)
        right = longest_common_substring(s1, s2, pos1, pos2+1, cache)
        cache[pos1][pos2] = right
        if len(left) > len(right):
            cache[pos1][pos2] = left

    return cache[pos1][pos2]
