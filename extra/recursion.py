#!/usr/bin/env python3

def knapsack_helper(capacity, items, position=0, curr_capacity=0, curr_value=0, values=[]):
    if position == len(items):
        values.append(curr_value)
    else:
        size, value = items[position]
        if size + curr_capacity <= capacity:
            knapsack_helper(capacity, items, position=position+1,
                    curr_capacity=curr_capacity+size, curr_value=curr_value+value, 
                    values=values)
        knapsack_helper(capacity, items, position=position+1,
                curr_capacity=curr_capacity, curr_value=curr_value,
                values=values)

def knapsack(capacity, items):
    values = []
    knapsack_helper(capacity, items, values=values)
    return max(values)
