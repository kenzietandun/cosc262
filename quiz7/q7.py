#!/usr/bin/env python3

"""A broken implementation of a recursive search for the optimal path through
   a grid of weights.
   Richard Lobb, January 2019.
"""
from math import inf as INFINITY

def read_grid(filename):
    """Read from the given file an n x m grid of integer weights.
       The file must consist of n lines of m space-separated integers.
       n and m are inferred from the file contents.
       Returns the grid as an n element list of m element lists.
       THIS FUNCTION DOES NOT HAVE BUGS.
    """
    with open(filename) as infile:
        lines = infile.read().splitlines()

    grid = [[int(bit) for bit in line.split()] for line in lines]
    return grid

## TOP DOWN SOLUTION
#def grid_cost(grid):
#    """The cheapest cost from row 1 to row n (1-origin) in the given
#       grid of integer weights.
#    """
#    n_rows = len(grid)
#    n_cols = len(grid[0])
#    cache = [[None for i in range(n_cols)] for j in range(n_rows)]
#    
#    def cell_cost(row, col, cache):
#        """The cost of getting to a given cell in the current grid."""
#        if row < 0 or row >= n_rows or col < 0 or col >= n_cols:
#            return INFINITY  # Off grid cells are treated as infinities
#        elif cache[row][col] is not None:
#            return cache[row][col]
#        else:
#            cost = grid[row][col]
#            if row != 0:
#                cost += min(cell_cost(row - 1, col + delta_col, cache) for delta_col in range(-1, +2))
#            cache[row][col] = cost
#            return cost
#            
#    best = min(cell_cost(n_rows - 1, col, cache) for col in range(n_cols))
#    return best

# BOTTOM UP SOLUTION
def grid_cost(grid):
    """The cheapest cost from row 1 to row n (1-origin) in the given
       grid of integer weights.
    """
    n_rows = len(grid)
    n_cols = len(grid[0])
    for i in range(n_rows):
        for j in range(n_cols):
            if i == 0:
                # leave the first row alone
                continue 
            # WARNING: MAGICAL
            # checks from the second row, the min cost of getting to 
            # that grid must be the minimal cost from the previous 
            # adjacent grid plus the cost of the grid
            grid[i][j] = grid[i][j] + min([grid[i-1][j + delta_col] 
                for delta_col in range(-1, 2) 
                if j + delta_col >= 0 and j + delta_col < n_cols])

    best = min(grid[n_rows - 1])

    return best
    
def file_cost(filename):
    """The cheapest cost from row 1 to row n (1-origin) in the grid of integer
       weights read from the given file
    """
    return grid_cost(read_grid(filename))

class Item:
    """An item to (maybe) put in a knapsack"""
    def __init__(self, value, weight):
        self.value = value
        self.weight = weight

def max_value(items, capacity):
    """The maximum value achievable with a given list of items and a given
       knapsack capacity."""
    num_cols = capacity + 1
    num_rows = len(items) + 1
    grid = [[0 for j in range(num_cols)] for i in range(num_rows)]
    for i in range(num_rows):
        if i == 0:
            continue
        for j in range(num_cols):
            curr_item_weight = items[i-1].weight
            max_value = grid[i-1][j]
            if j >= curr_item_weight:
                max_value = max(max_value, items[i-1].value + grid[i-1][j-curr_item_weight])
            grid[i][j] = max_value

    return grid[-1][-1]

def knapsack(capacity, items, pos=0, cache=None):
    if cache is None:
        cache = [[None for i in range(capacity + 1)] for j in range(len(items) + 1)]

    if pos >= len(items):
        return 0
    elif cache[pos][capacity] is not None:
        return cache[pos][capacity]
    else:
        best = knapsack(capacity, items, pos+1, cache=cache)
        item_weight, item_value = items[pos]
        if capacity >= item_weight:
            best = max(best, item_value + knapsack(capacity - item_weight, items, pos+1, cache=cache))

        cache[pos][capacity] = best

    return cache[pos][capacity]

def coins_reqd(value, coinage):
    """The minimum number of coins to represent value"""
    numCoins = [[(min(coinage), 0)]] * (value + 1)
    for amt in range(min(coinage), value + 1):
        coins_combi = [incr_coin(numCoins[amt - coin], coin) 
                for coin in coinage if amt >= coin]
        min_coins = least_coins_amount(coins_combi)
        numCoins[amt] = min_coins

    for index, i in enumerate(numCoins):
        print("{}:  {}".format(index, i))
    return sorted(numCoins[value], key=lambda x: x[0], reverse=True)

def incr_coin(coins_list, coin):
    new_coins_list = []
    found = False
    for denomination, count in coins_list:
        if count == 0:
            continue
        if denomination == coin:
            new_coins_list.append((denomination, count + 1))
            found = True
        else:
            new_coins_list.append((denomination, count))

    if not found:
        new_coins_list.append((coin, 1))

    return new_coins_list

def least_coins_amount(coins_list):
    return min((c for c in coins_list),
            key=lambda x: sum(total for _, total in x))


